
from rest_framework import status, generics
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from backend.serializers import UserSerializer, UserSerializerWithToken


class registerView(generics.CreateAPIView):
    serializer_class = UserSerializerWithToken

    def Post(self, request):
        serializer = UserSerializerWithToken(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


